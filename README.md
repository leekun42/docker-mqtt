# docker-mqtt

#### 介绍
    docker搭建mqtt服务器
    文档：https://docs.emqx.cn/broker/latest/

#### 软件架构
    mqtt单主机，多容器集群，nginx负载均衡

#### 使用说明
    首先安装docker-compose
    切换到本项目根目录，执行 docker-compose up -d
 
#### 配置说明
    mqtt使用mysql认证参考： https://docs.emqx.cn/broker/latest/advanced/auth-mysql.html#mysql-%E8%BF%9E%E6%8E%A5%E4%BF%A1%E6%81%AF